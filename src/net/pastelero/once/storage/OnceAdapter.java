/*
 * Copyright 2012, Angel Abad
 *
 * This file is part of Once.
 *
 * Once is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Once is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Once.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.pastelero.once.storage;

import net.pastelero.once.R;
import android.content.Context;
import android.database.Cursor;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class OnceAdapter extends SimpleCursorAdapter {

	// Constantes para vincular los datos con la vista
	static final String[] FROM = { DataStorage.C_NUMBER, DataStorage.C_SERIE, DataStorage.C_DATE, DataStorage.C_CHECK, DataStorage.C_TYPE };
	static final int[] TO = { R.id.dbNumber, R.id.dbSerie };

	public OnceAdapter(Context context, Cursor c) {
		super(context, R.layout.row, c, FROM, TO);
	}

	@Override
	public void bindView(View row, Context context, Cursor cursor) {
		super.bindView(row, context, cursor);

		// Vinculamos a mano la fecha para poder formatearla
		long timestamp = cursor.getLong(cursor.getColumnIndex(DataStorage.C_DATE));
		TextView dbFecha = (TextView) row.findViewById(R.id.dbFecha);
		dbFecha.setText(DateUtils.getRelativeTimeSpanString(timestamp));

		// Asignamos icono segun el valor de check
		ImageView dbImagen = (ImageView) row.findViewById(R.id.dbImagen);
		int checkValue = cursor.getInt(cursor.getColumnIndex(DataStorage.C_CHECK));

		if (checkValue == 0) {
			dbImagen.setImageResource(R.drawable.img_bad);
		} else if (checkValue == 1) {
			dbImagen.setImageResource(R.drawable.img_good);
		} else if (checkValue == -1) {
			dbImagen.setImageResource(0);
		}

		// Mostramos el tipo de cupón
		// TODO Sacar de aqui para poder usarlo en numberdetails tambien. 
		TextView dbTipo = (TextView) row.findViewById(R.id.dbTipo);
		String type = cursor.getString(cursor.getColumnIndex(DataStorage.C_TYPE));
		if (type.equals("NORMAL") || type.equals("N")) {
			dbTipo.setText(context.getString(R.string.DiaryCoupon));
		} else if (type.equals("VIERNES") || type.equals("V")) {
			dbTipo.setText(context.getString(R.string.CouponPlus));
		} else if (type.equals("FINDE") || type.equals("D")) {
			dbTipo.setText(context.getString(R.string.WeekendCoupon));
		}

	}



}

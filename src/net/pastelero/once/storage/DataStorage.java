/*
 * Copyright 2012, Angel Abad
 *
 * This file is part of Once.
 *
 * Once is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Once is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Once.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.pastelero.once.storage;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import net.pastelero.once.Number;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

public class DataStorage extends SQLiteOpenHelper {

	private static final String TAG = DataStorage.class.getSimpleName();
	static final String DB_NAME = "once.db";
	static final int DB_VERSION = 1;
	static final String TABLE = "once";
	static final String C_ID = BaseColumns._ID;
	static final String C_NUMBER = "number";
	static final String C_SERIE = "serie";
	static final String C_DATE = "date";
	static final String C_CHECK = "checked";
	static final String C_TYPE = "type";
	//Context context;

	private static final String GET_ALL_ORDER_BY = C_DATE + " DESC";

	// Constructor
	public DataStorage(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
		//this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String sql = "create table " + TABLE + " (" + C_ID + " INTEGER PRIMARY KEY, "
				+ C_NUMBER + " VARCHAR(5), "
				+ C_SERIE + " VARCHAR(3), "
				+ C_DATE + " INTEGER, "
				+ C_CHECK + " INTEGER DEFAULT '-1', "
				+ C_TYPE + " VARCHAR(7))";

		db.execSQL(sql);

		Log.d(TAG, "Database created: " + sql);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("drop table if exists " + TABLE);
		Log.d(TAG, "Database updated");
		onCreate(db);
	}

	// Add new number
	public void addNumber (Number number) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(DataStorage.C_NUMBER, number.getNumber());
		values.put(DataStorage.C_SERIE, number.getSerie());
		values.put(DataStorage.C_DATE, number.getDate());
		values.put(DataStorage.C_TYPE, number.getType());

		db.insertOrThrow(DataStorage.TABLE, null, values);
		db.close();

		Log.d(TAG, "Number added");
	}

	public void addBarcodeNumber (String barcodeNumber) throws ParseException {

		long date;
		String serie = barcodeNumber.substring(21, 24);
		String numero = barcodeNumber.substring(16 , 21);

		// El año viene solo con los dos ultimos digitos
		Integer ano = Integer.parseInt("20" + barcodeNumber.substring(14, 16));
		Integer mes = Integer.parseInt(barcodeNumber.substring(12, 14));
		Integer dia = Integer.parseInt(barcodeNumber.substring(10, 12));
		
		// Obtiene la fecha en unixtime del datepicker
		// TODO Abstraer esta funcion de calendar
		// TODO Se usa tambien en AddNumberActivity
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, dia);
		// Por alguna extraña razón los meses empiezan en cero
		// así que hay que restarle uno.
		calendar.set(Calendar.MONTH, mes - 1);
		calendar.set(Calendar.YEAR, ano);
		calendar.set(Calendar.HOUR_OF_DAY, 21);
		calendar.set(Calendar.MINUTE, 30);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		date = calendar.getTimeInMillis();
		
		Number number = new Number(numero, serie, date);
		addNumber(number);
	}

	// Get number by id
	public Number getNumber(int id) {

		SQLiteDatabase db = this.getReadableDatabase();
		Number number;

		String[] campos = new String[] { DataStorage.C_ID, DataStorage.C_NUMBER, DataStorage.C_SERIE, DataStorage.C_DATE, DataStorage.C_CHECK };
		String[] args = new String[] { String.valueOf(id) };
		Cursor cursor = db.query(DataStorage.TABLE, campos, "_id=?", args, null, null, null);

		if(cursor != null)
			cursor.moveToFirst();

		number = new Number(
				Integer.parseInt(cursor.getString(0)),
				cursor.getString(1),
				cursor.getString(2),
				Long.parseLong(cursor.getString(3)),
				cursor.getInt(4)
				);

		db.close();

		return number;
	}

	public Cursor getAllNumbers() {
		SQLiteDatabase db = this.getReadableDatabase();

		// TODO La base de datos se queda abierta
		Cursor cursor = db.query(DataStorage.TABLE, null, null, null, null, null, GET_ALL_ORDER_BY);
		//db.close();

		return cursor;
	}

	public List<Number> getNotCheckedNumbers() {
		SQLiteDatabase db = this.getReadableDatabase();
		ArrayList<Number> numbers = new ArrayList<Number>();

		int checkData = -1;

		String[] campos = new String[] { DataStorage.C_ID, DataStorage.C_NUMBER, DataStorage.C_SERIE, DataStorage.C_DATE, DataStorage.C_CHECK };
		String[] args = new String[] { String.valueOf(checkData) };
		Cursor cursor = db.query(DataStorage.TABLE, campos, "checked=?", args, null, null, null);

		if (cursor.moveToFirst()) {
			//Recorremos el cursor hasta que no haya más registros
			do {
				// Obetenemos el numero segun su id
				// TODO Quizas se pueda optimizar
				numbers.add(getNumber(Integer.parseInt(cursor.getString(0))));

			} while(cursor.moveToNext());
		}

		db.close();

		return numbers;
	}

	public void deleteNumber(Number number) {
		SQLiteDatabase db = this.getWritableDatabase();

		db.delete(DataStorage.TABLE, "_id=?", new String[] { String.valueOf(number.getId())});

		db.close();
	}

	public void updateCheck(Number number, int check) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(DataStorage.C_CHECK, check);

		db.update(DataStorage.TABLE, values, "_id=?", new String[] { String.valueOf(number.getId())});

		db.close();
	}
}

/*
 * Copyright 2012, Angel Abad
 *
 * This file is part of Once.
 *
 * Once is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Once is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Once.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.pastelero.once;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SearchByDayActivity extends SherlockActivity {

	TextView txtNumber;
	TextView txtHeaderSerie;
	TextView txtSerie;
	TextView txtDate;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.numberdetails);
		
		String number;
		String serie;
		String date;
		
		// TODO Mirar si esto se puede hacer en el XML
		// Que actionbar tenga atras en el icono
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE|ActionBar.DISPLAY_SHOW_HOME|ActionBar.DISPLAY_HOME_AS_UP);
		
		// Obtenemos los datos extra del datepicker
		number = getIntent().getStringExtra("number");
		serie = getIntent().getStringExtra("serie");
		date = getIntent().getStringExtra("date");
		
		// Asignamos las vistas
		txtNumber = (TextView) findViewById(R.id.detailsNumber);
		txtHeaderSerie = (TextView) findViewById(R.id.detailsHeaderSerie);
		txtSerie = (TextView) findViewById(R.id.detailsSerie);
		txtDate = (TextView) findViewById(R.id.detailsFecha);
		
		// Asignamos los datos a las vistas
		txtNumber.setText(number);
		if(serie == null) {
			txtHeaderSerie.setVisibility(View.GONE);
			txtSerie.setVisibility(View.GONE);
		} else {
			txtSerie.setText(serie);
		}
		txtDate.setText(date);
	}
	
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }

}

/*
 * Copyright 2012, Angel Abad
 *
 * This file is part of Once.
 *
 * Once is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Once is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Once.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.pastelero.once;

import java.util.Calendar;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import net.pastelero.once.R;
import net.pastelero.once.storage.DataStorage;
import net.pastelero.once.ui.CustomDatePicker;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.EditText;

public class AddNumberActivity extends SherlockActivity {

	protected static final String TAG = "ADDNUMBER";
	EditText editNumber;
	EditText editSerie;
	CustomDatePicker editFecha;

	DataStorage dataStorage;

	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.addnumber);

		// Elimina el icono de actionbar
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayShowHomeEnabled(false);

		dataStorage = new DataStorage(this);

		editNumber = (EditText) findViewById(R.id.editNumber);
		editSerie = (EditText) findViewById(R.id.editSerie);
		editFecha = (CustomDatePicker) findViewById(R.id.editFecha);
		// Desactivar el calendario en versiones superiores de Android
		if(android.os.Build.VERSION.SDK_INT >= 11) {
			editFecha.setCalendarViewShown(false);
		}

	}

	// Creacion del menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.addnumber, menu);
		return true;
	}

	//TODO Convertir las cosas largas en funciones
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;
		case R.id.addNumberCangelButton:
			finish();
			break;
		case R.id.addNumberButton:
    		// Comprobamos el tamaño de los numeros, si es correcto introducimos
    		if(editNumber.getText().toString().length() < 5) { 
    			editNumber.setError(getString(R.string.Number5Digits));
    		} else if (editSerie.getText().toString().length() < 3) {
    			editSerie.setError(getString(R.string.Serie3Digits));
    		} else {
    			// Obtiene la fecha en unixtime del datepicker
    			// TODO Abstraer esta funcion de calendar
    			// TODO Se usa tambien DataStorage
    			Calendar calendar = Calendar.getInstance();
    			calendar.set(Calendar.DAY_OF_MONTH, editFecha.getDayOfMonth());
    			calendar.set(Calendar.MONTH, editFecha.getMonth());
    			calendar.set(Calendar.YEAR, editFecha.getYear());
    			calendar.set(Calendar.HOUR_OF_DAY, 21);
    			calendar.set(Calendar.MINUTE, 30);
    			calendar.set(Calendar.SECOND, 0);
    			calendar.set(Calendar.MILLISECOND, 0);
    			long date = calendar.getTimeInMillis();

    			String number = editNumber.getText().toString();
    			String serie = editSerie.getText().toString();

    			// Crear en la base de datos
    			dataStorage.addNumber(new Number(number, serie, date));

    			finish();
    		}
    		break;
		}

		return true;
	}

}

/*
 * Copyright 2012, Angel Abad
 *
 * This file is part of Once.
 *
 * Once is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Once is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Once.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.pastelero.once;

import java.io.IOException;
import java.util.Date;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import net.pastelero.once.storage.DataStorage;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class NumberDetailsActivity extends SherlockActivity {

	private TextView detailsNumber;
	private TextView detailsSerie;
	private TextView detailsFecha;

	private TextView detailsPremio;

	private int id;
	private Number number;

	ProgressDialog checkDialog;

	private DataStorage dataStorage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.numberdetails);

		// TODO Mirar si esto se puede hacer en el XML
		// Que actionbar tenga atras en el icono
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE|ActionBar.DISPLAY_SHOW_HOME|ActionBar.DISPLAY_HOME_AS_UP);

		detailsNumber = (TextView) findViewById(R.id.detailsNumber);
		detailsSerie = (TextView) findViewById(R.id.detailsSerie);
		detailsFecha = (TextView) findViewById(R.id.detailsFecha);

		// Obtiene el numero desde la base de datos
		dataStorage = new DataStorage(this);
		id = getIntent().getIntExtra("NUMBER_ID", 0);
		number = dataStorage.getNumber(id);

		detailsNumber.setText(number.getNumber());
		detailsSerie.setText(number.getSerie());
		detailsFecha.setText(number.getShortFormatDate(this));

		detailsPremio = (TextView) findViewById(R.id.detailsPremio);
		if(number.getCheck() == 0) {
			detailsPremio.setText(getString(R.string.NOTAWARDED));
		} else if (number.getCheck() == 1) {
			detailsPremio.setText(getString(R.string.AWARDED));
		}

	}

	// Creacion del menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.details, menu);
		return true;
	}

	//TODO Convertir las cosas largas en funciones
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;
		case R.id.btnDetailsCheckNumber:
			// Hora actual
			Date now = new Date();
			long nowLong = now.getTime();

			// Chequea si ya ha pasado la hora del sorteo
			if (number.getDate() > nowLong) {
				Toast.makeText(getApplicationContext(), getString(R.string.NoDataYet), Toast.LENGTH_SHORT).show();
			} else {
				new DetailsCheckNumber().execute();
			}
			break;
		case R.id.btnDetailsDeleteNumber:
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(NumberDetailsActivity.this);
			alertDialog.setTitle(getString(R.string.AreYouSure));
			alertDialog.setMessage(getString(R.string.DoYouWantToRemove) + " " + number.getNumber() + "?");

			alertDialog.setPositiveButton(getString(R.string.YES), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {

					// Borra el numero de la base de datos
					dataStorage.deleteNumber(number);

					String message = number.getNumber() + " - " + number.getSerie() + " " + getString(R.string.deleted);
					Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

					finish();
				}
			});

			alertDialog.setNegativeButton(getString(R.string.NO), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
				}
			});
			alertDialog.show();
			break;
		}

		return true;
	}

	private class DetailsCheckNumber extends AsyncTask<Void, Integer, Integer> {

		@Override
		protected void onPreExecute() {
			checkDialog = ProgressDialog.show(NumberDetailsActivity.this,
					getString(R.string.Updating),
					getString(R.string.VerifyingNumber));
		}

		@Override
		protected Integer doInBackground(Void... params) {

			int checkResult = 3;

			// TODO Meter el webcheck dentro del Utils.isOnline
			WebCheck webCheck = new WebCheck();
			
			// TODO Quizas sea mejor hacer la comprobacion de online en checkNumber
			if(Utils.isOnline() == true) {
				try {
					checkResult = webCheck.checkByNumber(number);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				checkResult = 3;
			}

			return checkResult;
		}

		@Override
		protected void onPostExecute(Integer result) {
			checkDialog.dismiss();

			// TODO Todo esto deberia ir en el doInBackground
			// Chequeado
			if(result == 1 || result == 0) {
				// Recargamos toda la vista sin transicion ni animacion
				Intent intent = getIntent();
				overridePendingTransition(0, 0);
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				finish();
				overridePendingTransition(0, 0);
				startActivity(intent);
				// No disponible todavia
			} else if(result == 2) {
				Toast.makeText(getApplicationContext(), getString(R.string.NoDataYet), Toast.LENGTH_LONG).show();
				// No internet
			} else if(result == 3) {
				Toast.makeText(getApplicationContext(), getString(R.string.NoInternetConnected), Toast.LENGTH_LONG).show();
			}
		}

	}

}

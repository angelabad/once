/*
 * Copyright 2012, Angel Abad
 *
 * This file is part of Once.
 *
 * Once is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Once is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Once.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.pastelero.once;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Utilidades varias para no repetir codigo en las demas
 * clases.
 * @author Angel Abad <angelabad@gmail.com>
 *
 */
public class Utils {

	/**
	 * <p>Convierte un String con el numero en un ArrayList con
	 * los numeros por separado.</p>
	 * <p>Se utiliza String para los numeros porque Integer no
	 * permite guardar 0</p>
	 * @param number String con el numero.
	 * @return Array de Strings con los numeros separados.
	 */
	protected static List<String> numberToArray(String number) {
		List<String> digits = new ArrayList<String>();

		char[] cArray = number.toCharArray();
		for (int i = 0; i < cArray.length; i++) {
			digits.add(String.valueOf(cArray[i]));
		}

		return digits;	
	}

	public static boolean isOnline() {

		Context context = OnceApplication.getAppContext();

		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(
				Context.CONNECTIVITY_SERVICE);

		NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
		if(networkInfo!=null && networkInfo.isAvailable() && networkInfo.isConnected()) {
			return true;
		} else {
			return false;
		}
	}

	public static void awardNotification(boolean award, String number) {
		CharSequence description = null;
		Context context = OnceApplication.getAppContext();
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

		int icon = android.R.drawable.stat_sys_download_done;;

		// TODO Hacer cadenas traducibles
		if (award == true) {
			description = "El número " + number + " ha sido premiado";
		} else {
			description = "El número " + number + " no ha sido premiado";
		}

		CharSequence awardText = number + " actualizado";
		long time = System.currentTimeMillis();

		Notification notification = new Notification(icon, awardText, time);

		// Texto a mostrar al desplegar
		CharSequence title = "Premios Once";

		// PendingIntent a lanzar al pulsar en la notificacion
		Intent notificationIntent = new Intent(context, OnceActivity.class);
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

		// Configuramos notificacion
		notification.setLatestEventInfo(context, title, description, contentIntent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		// Enviar notificacion
		notificationManager.notify(Integer.parseInt(number), notification);
	}
	
	// TODO Esto está repetido en number, hacerlo solo una vez.
	protected static String getDayWebCode(long longDate) {
		int dayOfWeek;
		String result = null;
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(longDate);
		dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

		// Dias entre semana
		if (dayOfWeek == 2 || dayOfWeek == 3 || dayOfWeek == 4 || dayOfWeek == 5) {
			result = "N";
			// Sabado y domingo
		} else if (dayOfWeek == 7 || dayOfWeek == 1) {
			result = "D";
			// Viernes
		} else if (dayOfWeek == 6) {
			result ="V";
		}
		
		return result;
	}
}

/*
 * Copyright 2012, Angel Abad
 *
 * This file is part of Once.
 *
 * Once is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Once is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Once.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.pastelero.once.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class UpdaterAlarmReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d("RECEIVER", "Recurring alarm, requesting service");

		context.startService(new Intent(context, UpdaterService.class));

	}

	public static void setUpdaterAlarm(Context context) {

		// Intervalo entre checkeos - 45 Minutos
		long interval = 1000 * 60 * 45;

		// TODO Se podria lanzar con startService?
		Intent updaterAlarmReceiver = new Intent(context, UpdaterAlarmReceiver.class);
		PendingIntent updaterAlarm = PendingIntent.getBroadcast(context,
				0, updaterAlarmReceiver, PendingIntent.FLAG_CANCEL_CURRENT);
		AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

		alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP,
				System.currentTimeMillis(), interval, updaterAlarm);
	}

}

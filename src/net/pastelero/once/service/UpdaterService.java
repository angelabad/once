/*
 * Copyright 2012, Angel Abad
 *
 * This file is part of Once.
 *
 * Once is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Once is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Once.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.pastelero.once.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import net.pastelero.once.WebCheck;
import net.pastelero.once.OnceApplication;
import net.pastelero.once.Utils;
import net.pastelero.once.storage.DataStorage;
import net.pastelero.once.Number;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

public class UpdaterService extends Service {

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		UpdaterTask updaterTask = new UpdaterTask();
		updaterTask.execute();

		return Service.START_FLAG_REDELIVERY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	private class UpdaterTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {

			Log.d("SERVICE", "Mensaje recibido");

			DataStorage db = new DataStorage(OnceApplication.getAppContext());
			List<Number> numbers = db.getNotCheckedNumbers();

			// Hora actual
			Date now = new Date();
			long nowLong = now.getTime();

			// Ver si estan activadas las notificaciones
			SharedPreferences prefs = OnceApplication.getPrefs();
			boolean notificationsActivated = prefs.getBoolean("notifications", true);

			for (Number number: numbers) {
				if (number.getDate() < nowLong) {
					if(Utils.isOnline() == true) {
						try {

							int checkResult = new WebCheck().checkByNumber(number);
							
							// Premio
							if (checkResult == 1) {
								if (notificationsActivated == true) 
									Utils.awardNotification(true, number.getNumber());
							} else if (checkResult == 0) {
								if (notificationsActivated == true)
									Utils.awardNotification(false, number.getNumber());
							}

							// Envia el receiver para que se actualice la vista principal
							// TODO Sustituir la cadena por una constante.
							// TODO Igual habria que saber si estamos en OnceActivity para hacer esto y no lanzar las notificaciones.
							Intent intent = new Intent("net.pastelero.once.NEW_CHECK");
							sendBroadcast(intent);

						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}

			return null;
		}
	}

}

/*
 * Copyright 2012, Angel Abad
 *
 * This file is part of Once.
 *
 * Once is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Once is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Once.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.pastelero.once;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import net.pastelero.once.storage.DataStorage;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class WebCheck {
	
	private static final String CHECK_URL = "http://www.juegosonce.com/wmx/dicadi/pub/premEstadistic/detalleEscrutiniocupon.cfm?fecha=%s&tiposorteo=%s";

	// TODO Chequear si el numero es nulo
	private Number inetCheck(long longDate) throws IOException {
		Number resultNumber;
		String inetNumber = null;
		String inetSerie = null;
		String date;
		
		Document doc = null;
		String checkUrl;
		
		// Formatea la fecha para la web
		SimpleDateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
		date = formatDate.format(new Date(longDate));
		
		checkUrl = String.format(CHECK_URL, date, Utils.getDayWebCode(longDate));

		try {
			doc = Jsoup.connect(checkUrl).get();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// Obtenemos primer numero
		try {
			inetNumber = doc.select("div.combGrandeN1").first().text();
		} catch (NullPointerException e) {
			return null;
		}
		
		// Obtenemos los numeros siguientes
		Elements netDigits = doc.select("div.combGrandeN2");
		for(Element digit: netDigits) {
			inetNumber += digit.text();
 		}
		
		// Obtenemos la serie
		// TODO que esto genere bien las excepciones
		try {
			inetSerie = doc.select("span.serieNumC").first().text();
		} catch (Exception e) {
			//e.printStackTrace();
		}
		
		// Asignamos a resultNumber los datos mas la fecha pasada por parametro
		resultNumber = new Number(inetNumber, longDate);
		if (inetSerie != null) {
			resultNumber.setSerie(inetSerie);
		}
		
		return resultNumber;
	}
	
	public int checkByNumber(Number number) throws IOException {

		// El resultado de la operación, por defecto No comparado
		int result = 2;
		Number inetNumber;
		DataStorage dataStorage = new DataStorage(OnceApplication.getAppContext());

		inetNumber = inetCheck(number.getDate());

		System.out.println("Entrado en equals");
		List<String> inetNumberDigits = Utils.numberToArray(inetNumber.getNumber());
		List<String> localNumberDigits = Utils.numberToArray(number.getNumber());

		// Chequeamos si coincide el ultimo numero
		if(localNumberDigits.get(4).equals(inetNumberDigits.get(4))) {
			dataStorage.updateCheck(number, 1);
			result = 1;
			// SI es diario checqueamos tambien el primer numero
			// TODO aqui se usa el getType de number, en otros sitios se usa el valor en la base de datos. (En adapter)
		} else if (number.getType().equals("N") && localNumberDigits.get(0).equals(inetNumberDigits.get(0))) {
			dataStorage.updateCheck(number, 1);
			result = 1;
		} else {
			dataStorage.updateCheck(number, 0);
			result = 0;
		}

		return result;
	}
	
	public Number checkByDate(int year, int monthOfYear, int dayOfMonth) throws IOException {
		Number number;
		long date;
		
		// Convertimos en long date
		final Calendar c = Calendar.getInstance();
		c.set(year, monthOfYear, dayOfMonth);
		date = c.getTimeInMillis();
		
		number = inetCheck(date);
		
		return number;
	}
}

/*
 * Copyright 2012, Angel Abad
 *
 * This file is part of Once.
 *
 * Once is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Once is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Once.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.pastelero.once;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.content.Context;

public class Number {

	private int id;
	private String number;
	private String serie;
	private long date;
	private int check;

	// Constructor vacio
	public Number() { }

	// Constructor
	public Number(int id, String number, String serie, long date, int check) {
		this.id = id;
		this.number = number;
		this.serie = serie;
		this.date = date;
		this.check = check;
	}

	// Constructor
	public Number(String number, String serie, long date) {
		this.number = number;
		this.serie = serie;
		this.date = date;
	}

	// Constructor
	public Number(String number, long date) {
		this.number = number;
		this.date = date;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getType() {

		int dayOfWeek;
		String type = null;

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(this.date);
		dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

		// Dias entre semana
		if (dayOfWeek == 2 || dayOfWeek == 3 || dayOfWeek == 4 || dayOfWeek == 5) {
			type = "N";
		// Sabado y domingo
		} else if (dayOfWeek == 7 || dayOfWeek == 1) {
			type = "D";
		// Viernes
		} else if (dayOfWeek == 6) {
			type ="V";
		}

		return type;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public int getCheck() {
		return check;
	}

	public String getShortFormatDate(Context context) {
		Date date = new Date(this.date);
		DateFormat df = android.text.format.DateFormat.getDateFormat(context);
		String formatFecha = df.format(date);

		return formatFecha;
	}

	public String getDayNumber() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd");

		return dateFormat.format(new Date(this.date));
	}

	public String getMonthNumber() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM");

		return dateFormat.format(new Date(this.date));
	}

	public String getYearNumber() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");

		return dateFormat.format(new Date(this.date));
	}

}
